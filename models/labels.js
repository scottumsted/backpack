steal ('can', function ( can ) {
    return can.Model ( {
        findAll: 'GET /backpackapi/api/labels/{retailer}/{rma}'
    }, {} );
});