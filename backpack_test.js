steal(
	'funcunit',
	function (S) {

	// this tests the assembly 
	module("backpack", {
		setup : function () {
			S.open("//backpack/index.html");
		}
	});

	test("welcome test", function () {
		equals(S("h1").text(), "Welcome to JavaScriptMVC!", "welcome text");
	});

});
