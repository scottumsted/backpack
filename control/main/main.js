steal('can',
    './init.ejs',
    './customer/customer',
    './fxo/fxo',
    function (can,
        initView,
        Customer,
        Fxo) {
        /**
         * @class depot/control/main
         * @alias Main
         */
        return can.Control(
            /** @Static */
            {
                defaults: {}
            },
            /** @Prototype */
            {
                init: function () {
                    this.element.html(initView({}));
                    $(".header_company").bind("click", function () {
                        window.location.href = "#!";
                    });
                },
                "button click": function (el, ev) {
                    if (typeof ($(el).attr('id')) !== 'undefined' &&
                        $.inArray($(el).attr('id'), ['customer',
                                                    'fxo']) >= 0) {
                        ev.preventDefault();
                        window.location.href = "#!" + $(el).attr('id');
                    }
                },
                "customer route": function () {
                    new Customer('#main-block');
                },
                "fxo route": function () {
                    new Fxo('#main-block');
                }
            });
    });