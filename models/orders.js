steal ('can', function ( can ) {
    return can.Model ( {
        findAll: 'GET /backpackapi/api/orders/{retailer}/{orderId}/{reason}'
    }, {} );
});