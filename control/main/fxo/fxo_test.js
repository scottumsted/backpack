steal('backpack/control/main/fxo','funcunit', function( Fxo, S ) {

	module("backpack/control/main/fxo", { 
		setup: function(){
			S.open( window );
			$("#qunit-test-area").html("<div id='fxo'></div>")
		},
		teardown: function(){
			$("#qunit-test-area").empty();
		}
	});
	
	test("updates the element's html", function(){
		new Fxo('#fxo');
		ok( $('#fxo').html() , "updated html" );
	});

});