//js backpack/scripts/doc.js

load('steal/rhino/rhino.js');
steal("documentjs", function(DocumentJS){
	DocumentJS('backpack/index.html', {
		out: 'backpack/docs',
		markdown : ['backpack', 'steal', 'jquerypp', 'can', 'funcunit'],
		parent : 'backpack'
	});
});