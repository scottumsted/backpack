steal('can',
    './init.ejs',
    './rma.ejs',
    '../../../models/orders.js',
    function (can, initView, rmaView, Orders) {
        /**
         * @class hack_customer/control/main/order
         * @alias Order
         */
        return can.Control(
            /** @Static */
            {
                defaults: {}
            },
            /** @Prototype */
            {
                init: function () {
                    this.element.html(initView({
                        message: "Hello World from Customer"
                    }));
                },

                "#oStart click": function (el, ev) {
                    ev.preventDefault();
                    var self = this;
                    Orders.findAll({
                        'orderId': $('#oOrderId').val(),
                        'retailer': $('#oRetailer').val(),
                        'reason': encodeURI($('#oReason').val())
                    }, function (data) {
                        self.element.html(rmaView({
                            item: data[0]
                        }));
                    }, function (xhr, a,b,c,d){
                        console.log('error');
                    });
                }
            });
    });