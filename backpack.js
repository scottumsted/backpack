steal(
    './control/main/main',
	'./backpack.less',
	'./models/fixtures/fixtures.js',
function(Main){
	    if(!window.console) {
        var console = {
            log : function(){}
        }
        window.console = console;
    }
    if(!steal.isRhino) {
        new Main('#main-block');
    }
})