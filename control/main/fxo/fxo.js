steal('can',
    './init.ejs',
    './label.ejs',
    '../../../models/labels.js',
    function (can, initView, labelView, Labels) {
        return can.Control(
            /** @Static */
            {
                defaults: {}
            },
            /** @Prototype */
            {
                init: function () {
                    this.element.html(initView({
                        message: "Hello World from Fxo"
                    }));
                },
                "#fStart click": function (el, ev) {
                    ev.preventDefault();
                    var self = this;
                    Labels.findAll({
                        'rma': $('#fRma').val(),
                        'retailer': $('#fRetailer').val()
                    }, function (data) {
                        self.element.html(labelView({
                            item: data[0]
                        }));
                    }, function (xhr, a, b, c, d) {
                        console.log('error');
                    });
                }
            });
    });