steal('backpack/control/main/customer','funcunit', function( Customer, S ) {

	module("backpack/control/main/customer", { 
		setup: function(){
			S.open( window );
			$("#qunit-test-area").html("<div id='customer'></div>")
		},
		teardown: function(){
			$("#qunit-test-area").empty();
		}
	});
	
	test("updates the element's html", function(){
		new Customer('#customer');
		ok( $('#customer').html() , "updated html" );
	});

});