steal('backpack/control/main','funcunit', function( Main, S ) {

	module("backpack/control/main", { 
		setup: function(){
			S.open( window );
			$("#qunit-test-area").html("<div id='main'></div>")
		},
		teardown: function(){
			$("#qunit-test-area").empty();
		}
	});
	
	test("updates the element's html", function(){
		new Main('#main');
		ok( $('#main').html() , "updated html" );
	});

});